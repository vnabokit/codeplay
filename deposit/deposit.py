import sys

def main(argv):

    if len(argv) != 5:
        print ("Use 4 parameters.\nExample: deposit.py 50000 12.5 1.5 28.6")
        sys.exit(0)

    uah_start = int(argv[1])
    rate_uah = float(argv[2])
    rate_usd = float(argv[3])
    exchrate = float(argv[4])
    exch_step = 0.1

    print ("\nInput data:")
    print ("    %d UAH is initial funds." % (uah_start) )
    print ("    %.2f%% is annual rate for UAH deposit." % (rate_uah) )
    print ("    %.2f%% is annual rate for USD deposit." % (rate_usd) )
    print ("    %.2f UAH per 1 USD is initial exchange rate." % (exchrate) )

    uah_end = uah_start + uah_start * (rate_uah / 100)
    usd_end = (uah_start / exchrate) + (uah_start / exchrate)*(rate_usd / 100)
    uah_to_usd_end = uah_end / exchrate
    
    print ("\nUAH deposit: %.2f UAH (or %.2f USD if 1 USD = %.2f UAH)." % (uah_end, uah_to_usd_end, exchrate))
    print ("USD deposit: %.2f USD." % (usd_end))

    cnt_loops = 0
    while True:
        uah_to_usd_end = uah_end / exchrate
        if usd_end > uah_to_usd_end or cnt_loops>200:
            break
        else:
            exchrate += exch_step
            cnt_loops += 1
        

    if cnt_loops>=200:
        print ("\nToo many loops. Pass other parameters.\nExample: deposit.py 50000 12.5 1.5 28.6")
    else:
        print ("\nThe edge exchange rate is:\n%.2f UAH per 1 USD." % (exchrate) )
        print ("Then UAH deposit (%.2f USD when convert) "
            "is close to USD deposit (%.2f USD)." % (uah_to_usd_end, usd_end))

if __name__ == "__main__":
   main(sys.argv)