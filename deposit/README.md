## Find an edge exchange rate

Find an edge exchange rate when an UAH deposit become to be less profitable then USD deposit.
Download signle file `deposit.py` and launch it with parameters.

Example of usage:
```
python deposit.py 20000 10 1.5 28.6
```

Explanation:
`20000 UAH` is initial amound for funding.
`10.0%` is annual rate for UAH deposit.
`1.5%` is annual rate for USD deposit.
`28.60 UAH` per 1 USD is initial exchange rate.